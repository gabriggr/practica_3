# EJERCICIOS -SPARK-SHELL

## Introducción
Para la realización de estos ejercicios vamos a utilizar  **spark-shell** en modo **local** como hicimos en la práctica de la sesión 3.

Todos los comandos se van a realizar sobre la tabla cliente **r_operations.t_client**

## EJERCICIO
1. Creamos un nuevo dataFrame **(df)** seleccionando las columnas: *client_id, name, surname, birth, sex, phone*
2. Utilizando **df** del punto anterior creamos un nuevo dataFrame **(dfSex)** reemplazando el valor del campo *sex*
```
M->Hombre
W->Mujer
```
3. Utilizando *dfSex* creamos un nuevo dataFrame *(dfCast)* casteando las columnas:
```
client_id->Integer
phone -> Integer
birth -> Date
```

4. Partiendo de *dfSex*, creamos un nuevo dataFrame *(dfFilter)* quedandonos únicamente con los clientes nacidos después de *1960*
5. Utilizando *dfFiter*, creamos un nuevo dataFrame *(dfFin)* renombrando las columnas:
```
client_id -> id
name->nombre
surname->apellido
birth->cumple
sex->sexo
phone->telefono
```
6. Finalmente guardamos el dataFrame *(dfFin)* en formato *json* en la directorio /tmp/cliente utilizando como campo de partición el *sexo*
### ***Mandarnos un correo gabriel.gallardo@bluetab.net con los comandos utizalidos en cada uno de los pasos ***
