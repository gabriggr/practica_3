# Spark-Shell #
El shell de Spark proporciona una forma sencilla de aprender la API, así como una herramienta poderosa para analizar datos de forma interactiva. Está disponible en Scala (que se ejecuta en Java VM y, por lo tanto, es una buena forma de usar las bibliotecas Java existentes) o Python.

### Inicialización Spark-Shell ###

*  Modo Local
```bash
spark-shell --master local
```

* Modo Cluster
```bash
spark-shell --master yarn
```

* Ejecución Script inicialización
```bash
spark-shell --master yarn -i file.scala
```

* Importando paquetes. Utilizando coordandas maven.
```bash
spark-shell --packages org.apache.spark:spark-avro_2.12:3.1.2
```

* Añadiendo jar
```bash
spark-shell --jar ejemplo.jar
```


### Commandos Spark-Shell ###

Para ejecutar comando dentro de la shell utilizamore ":"

* Mostrar todos los comandos :help
```bash
:help
```

* Para lanzar comandos externos desde las spark-shell 
```bash
:sh hdfs dfs -ls /
```

* Obtnemos el resultado de la resp\<num\> que devuelva el comando
```scala
res0.lines.foreach(println)
```

* Cargar ficheros scala, útil si estamos modificando y probando ficheros en scala.
```bash
:load test.scala
```

### Ejercicios Spark-Shell ###

* Lectura de DataFrame
```scala
val df = spark.read.table("r_operations.t_loan")
```

* Lectura de fichero directamente.
```scala
val dfFile= spark.read.format("avro").load("/curso/raw/operations/t_loan")
```

* Mostrar Datos y esquema de un DataFrame 
```scala
df.show()
dfFile.show()
df.show(5)
df.printSchema()
```

* Cuenta los datos de un DataFrame
```scala
df.count()
```

* Recuperar la primera fila de un DataFrame
```scala
df.head()
```
* Lista de columnas de un DataFrame
```scala
df.columns
```
* Seleccionar columnas
```scala
df.select("loan_id", "account_id").show()
```

* RDD de un DataFrame
```scala
val rdd = df.rdd
rdd.foreach(println)
```

* Importar funciones sql y clases para tipos
```scala
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
```

[API functions](https://spark.apache.org/docs/latest/api/scala/org/apache/spark/sql/functions$.html)

[Tipos de datos](https://spark.apache.org/docs/latest/api/scala/org/apache/spark/sql/types/index.html)


* Casteado de campos en Spark y uso de withColumn
```scala
df.printSchema

val df_formated = df.withColumn("duration", col("duration").cast(IntegerType))
df_formated.printSchema

// Otra forma
val df_formated = df.withColumn("duration", col("duration").cast("integer"))

val df_formated = df.withColumn("duration", col("duration").cast(IntegerType)).withColumn("payments", col("payments").cast(DoubleType)) 

df_formated.printSchema()
```
* Filtros
```scala
val df_filtered = df_formated.filter(col("status") === "A")
df_filtered.show()

val df_filtered = df_formated.where(col("status") === "A")
df_filtered.show()
```


* Asignación de valores con WithColumn
```scala
val df_total_pay = df_filtered.withColumn("total_payment", col("duration") * col("payments"))
df_total_pay.show

val df_final = df_formated.withColumn("total_payment", when(col("status") === "A", col("duration")*col("payments")).otherwise(lit(0.0)))
df_final.show()
```

* Escritura
```scala
import org.apache.spark.sql.SaveMode._
df_final.write.mode(Overwrite).parquet("/tmp/final_data_parquet")
spark.read.parquet("/tmp/final_data_parquet").count

df_final.write.mode(Append).parquet("/tmp/final_data_parquet")

// Leemos dataFrame escrito
spark.read.parquet("/tmp/final_data_parquet").count

```
